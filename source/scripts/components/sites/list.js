import TemplateElement from '../TemplateElement.js'
import {GET} from '../../api.js'

function setupTableBody (source) {
  const root = this.shadowRoot
  const template = root.querySelector(source)
  const clone = document.importNode(template.content, true)
  const tbody = root.querySelector('tbody')
  while (tbody.firstChild !== null) {
    tbody.removeChild(tbody.lastChild)
  }
  tbody.appendChild(clone)
}

class SitesList extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/sites/list.html'
  }

  async connectedCallback () {
    await super.connectedCallback()
    const root = this.shadowRoot

    setupTableBody.call(this, '#loading')

    try {
      const sites = await GET('/v2/sites')
      if (Array.isArray(sites) && sites.length > 0) {
        const tbody = root.querySelector('tbody')
        const template = root.querySelector('#site')
        while (tbody.firstChild !== null) {
          tbody.removeChild(tbody.lastChild)
        }
        for (const site of sites) {
          const row = document.importNode(template.content, true)

          const link = row.querySelector('a')
          link.textContent = site.domain
          link.href = `/sites/${encodeURIComponent(site.domain)}/`

          const modified = row.querySelector('td:nth-of-type(2) > time')
          modified.setAttribute('datetime', site.modified)
          modified.textContent = new Date(site.modified).toLocaleString()

          tbody.appendChild(row)
        }
      } else {
        setupTableBody.call(this, '#empty')
      }
    } catch (error) {
      setupTableBody.call(this, '#error')
      root.querySelector('#description').textContent = error.message
    }
  }
}

window.customElements.define('app-sites-list', SitesList)
export default SitesList
