# website 🌏

Two sites in one:

- **Public home page** is just static content, easily scraped and crawled by bots.
- **Dashboard web app** is a single page app for users of the service.

## Development

Spin up the static file server.

```
npm run build
npm run server
```

Generate the static site, and regenerate on changes.

```
npm run watch
```

## Production

```
npm run build
npm run deploy
```

## Browser Support

Chrome Canary v65 works fine

Safari v11 works fine

Firefox Developer Edition v58 requires some custom settings in `about:config`. Set the follow options to true:

```
dom.webcomponents.customelements.enabled
dom.webcomponents.enabled
dom.moduleScripts.enabled
```

## Ugly Hacks

All hail the glorious Web Components master race. Unfortunately compromises are made to support filthy peasant current browsers.

I jest, of course. One reason this website is built with Web Components is to evaluate its current status as a feasible frontend development platform. The glitches listed here are aimed to track browser implementation progress and help overcome current limitations.

### CSS `:host` selector

This is used to scope CSS variables. In the light DOM we can use `:root`. In the shadow DOM the we can use `:host` in Safari and Chrome but not yet in Firefox.

Bug: https://bugzilla.mozilla.org/show_bug.cgi?id=992245

Workaround: Wrap the shadow DOM contents in an element with ID "host".

Downside: Redundant container element.

### `<link rel="stylesheet" href="css">`

In earlier versions of Web Components it was not allowed to link external stylesheets. This was reconsidered and currently allowed in Safari and Chrome. Sadly Firefox does not yet support it.

Workaround: Use `<style> @import 'css'; </style>` inside the shadow DOM.

Downside: FOUC

### `originalTarget` on `<input>` in shadow DOM

This is a non-standard event property in Firefox. Works well to expose the pre-retargeted event target. But in a `click` event on am `<input>` field that is inside a shadow DOM, it returns a special object called `Inaccessible {}`. Any property access, even `x in obj`, throws an error.

```
Error: Permission denied to access property "foo"
```

Workaround: Avoid using this `event.originalTarget` property.

### Event Retargeting and Client-Side Routing

Client-side routing uses `pushState` instead of links and form submissions. But `<a>` elements are still used in the DOM because the user can then perform native actions like copying the URL, opening in new window, etc. The `click` event on the `<a>` element must be intercepted. In an `open` mode shadom DOM the `event.composedPath()` method lists the full ancestor chain, exposing the `<a>` element, and allowing it to be intercepted and turned into a `popstate` event.

However in a `closed`-mode shadow DOM events are retargeted. That makes it impossible to detect the `click` event on nested `<a>` elements, or `submit` event on nested `form` elements for that matter.

Workaround: Create an `<app-link>` custom element that HTML authors can wrap around `<a>` elements. The `app-link` traps the `click` event on its slotted `<a>` contents and fires a `popstate` event on the global window context. The client-side router handles the `popstate` event.

Note: It could be argued that navigation from within `closed`-mode shadow DOM should not be intercepted at all by the client-side router.

### Lack of support for `<slot>` in Firefox

Not sure how this can be worked around. It is a fundamental aspect of Shadow DOM. Polyfills exist, e.g. shadydom, but are extremely invasive.
