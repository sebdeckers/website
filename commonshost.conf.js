module.exports = () => ({
  https: {
    port: 4433
  },
  http: {
    redirect: false
  },
  hosts: [
    {
      directories: {
        trailingSlash: 'never'
      },
      fallback: {
        200: 'app.html'
      },
      manifest: 'serverpush.json'
    }
  ]
})
